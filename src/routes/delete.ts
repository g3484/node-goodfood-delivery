import express, { Request, Response } from 'express';
import {
  NotAuthorizedError,
  NotFoundError,
  DeliveryStatus,
  requireAuth,
} from '@goodfood/common';
import { Delivery } from '../models/delivery';
import { DeliveryDeletedPublisher } from '../events/publishers/delivery-deleted-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.delete(
  '/api/deliveries/:id',
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const delivery = await Delivery.findById(id);

    if (!delivery) {
      throw new NotFoundError();
    }

    if (delivery.delivererId !== req.currentUser!.id) {
      throw new NotAuthorizedError();
    }

    await delivery.delete();

    new DeliveryDeletedPublisher(natsWrapper.client).publish({
      id: delivery.id,
      orderId: delivery.orderId,
    });

    res.status(204).send({
      message: 'Delivery deleted',
    });
  }
);

export { router as deleteDeliveryRouter };
