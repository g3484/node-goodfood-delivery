import express from 'express';
import { requireAuth } from '@goodfood/common';
import { Delivery } from '../models/delivery';

const router = express.Router();

router.get('/api/deliveries/current', requireAuth, async (req, res) => {
  const deliveries = await Delivery.find({
    delivererId: req.currentUser!.id,
  });

  return res.status(200).send(deliveries);
});

export { router as currentDeliveryRouter };
