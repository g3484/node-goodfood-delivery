import express from 'express';
import { Order } from '../../models/order';
import { requireAuth, OrderStatus } from '@goodfood/common';

const router = express.Router();

router.get('/api/deliveries/stats', requireAuth, async (req, res) => {
  const orders = await Order.find({
    status: OrderStatus.Completed,
  });

  const count = orders.length;

  return res.status(200).send({ count });
});

export { router as ordersCompletedDeliveryRouter };
