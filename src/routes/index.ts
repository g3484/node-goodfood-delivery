import express from 'express';
import { Order } from '../models/order';
import { requireAuth, OrderStatus } from '@goodfood/common';

const router = express.Router();

router.get('/api/deliveries', requireAuth, async (req, res) => {
  const orders = await Order.find({
    status: OrderStatus.AwaitingDelivery,
  });

  return res.status(200).send(orders);
});

export { router as ordersAwaitingDeliveryRouter };
