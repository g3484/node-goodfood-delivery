import express, { Request, Response } from 'express';
import fs from 'fs';
import {
  NotAuthorizedError,
  NotFoundError,
  DeliveryStatus,
  requireAuth,
  BadRequestError,
} from '@goodfood/common';
import { Delivery } from '../models/delivery';
import { DeliveryCompletedPublisher } from '../events/publishers/delivery-completed-publisher';
import { natsWrapper } from '../nats-wrapper';
import { body } from 'express-validator';
import { upload } from '../config/multer';
import { cloudinary } from '../config/cloudinary';

const router = express.Router();

router.post(
  '/api/deliveries/:id',
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const delivery = await Delivery.findById(id);

    if (!delivery) {
      throw new NotFoundError();
    }

    if (delivery.delivererId !== req.currentUser!.id) {
      throw new NotAuthorizedError();
    }

    delivery.set({
      status: DeliveryStatus.Completed,
      image: {
        imageUrl: '',
        imageId: '',
      },
    });

    new DeliveryCompletedPublisher(natsWrapper.client).publish({
      id: delivery.id,
      orderId: delivery.orderId,
    });

    res.status(200).send({
      message: 'Delivery completed',
    });
  }
);

export { router as completeDeliveryRouter };
