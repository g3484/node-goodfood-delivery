import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import {
  BadRequestError,
  NotFoundError,
  OrderStatus,
  requireAuth,
  validateRequest,
} from '@goodfood/common';
import { Order } from '../models/order';
import { Delivery } from '../models/delivery';
import { DeliveryCreatedPublisher } from '../events/publishers/delivery-created-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.post(
  '/api/deliveries',
  requireAuth,
  [
    body('orderId').not().isEmpty(),
    body('delivererCoordinates').not().isEmpty().isObject(),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { orderId, delivererCoordinates } = req.body;

    const order = await Order.findById(orderId);

    if (!order) {
      throw new NotFoundError();
    }

    if (order.status === OrderStatus.Cancelled) {
      throw new BadRequestError('Cannot deliver for an cancelled order');
    }

    if (order.status === OrderStatus.DeliveryInProgress) {
      throw new BadRequestError(
        'Cannot deliver for an order already in delivery'
      );
    }

    const delivery = Delivery.build({
      delivererId: req.currentUser!.id,
      orderId: order.id,
      clientName: order.userName,
      clientAddress: order.address,
      delivererCoordinates: {
        latitude: delivererCoordinates.latitude,
        longitude: delivererCoordinates.longitude,
      },
    });

    await delivery.save();

    new DeliveryCreatedPublisher(natsWrapper.client).publish({
      id: delivery.id,
      orderId: delivery.orderId,
    });

    res.status(201).send({
      id: delivery.id,
    });
  }
);

export { router as createDeliveryRouter };
