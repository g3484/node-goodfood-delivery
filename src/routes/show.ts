import express from 'express';
import { NotAuthorizedError, NotFoundError } from '@goodfood/common';
import { Delivery } from '../models/delivery';
import { requireAuth } from '@goodfood/common';
import { Order } from '../models/order';

const router = express.Router();

router.get('/api/deliveries/:orderId', requireAuth, async (req, res) => {
  const { orderId } = req.params;

  const order = await Order.findById(orderId);

  if (!order) {
    throw new NotFoundError();
  }

  const delivery = await Delivery.find({
    orderId: orderId,
  });

  if (!delivery) {
    throw new NotFoundError();
  }

  res.status(200).send(delivery);
});

export { router as DeliveryByOrderIdRouter };
