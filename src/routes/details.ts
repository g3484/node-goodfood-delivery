import express from 'express';
import { NotFoundError } from '@goodfood/common';
import { requireAuth } from '@goodfood/common';
import { Order } from '../models/order';

const router = express.Router();

router.get('/api/deliveries/details/:orderId', requireAuth, async (req, res) => {
  const { orderId } = req.params;

  const order = await Order.findById(orderId);

  if (!order) {
    throw new NotFoundError();
  }

  res.status(200).send(order);
});

export { router as DeliveryOrderDetailsRouter };
