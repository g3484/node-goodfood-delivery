import express, { Request, Response } from 'express';
import {
  NotAuthorizedError,
  NotFoundError,
  validateRequest,
} from '@goodfood/common';
import { Delivery } from '../models/delivery';
import { requireAuth } from '@goodfood/common';
import { Order } from '../models/order';
import { body } from 'express-validator';

const router = express.Router();

router.patch(
  '/api/deliveries/:id',
  requireAuth,
  [body('delivererCoordinates').not().isEmpty().isObject()],
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const { delivererCoordinates } = req.body;

    const delivery = await Delivery.findById(id);

    if (!delivery) {
      throw new NotFoundError();
    }

    if (delivery.delivererId !== req.currentUser!.id) {
      throw new NotAuthorizedError();
    }

    delivery.set({
      delivererCoordinates: {
        latitude: delivererCoordinates.latitude,
        longitude: delivererCoordinates.longitude,
      },
    });

    await delivery.save();

    res.status(200).send(delivery);
  }
);

export { router as updateDeliveryRouter };
