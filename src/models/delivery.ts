import mongoose from 'mongoose';
import { DeliveryStatus } from '@goodfood/common';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';

export interface DeliveryAttrs {
  delivererId: string;
  orderId: string;
  clientName: string;
  clientAddress: string;
  delivererCoordinates: {
    latitude: number;
    longitude: number;
  };
}

export interface DeliveryDoc extends mongoose.Document {
  delivererId: string;
  orderId: string;
  clientName: string;
  clientAddress: string;
  delivererCoordinates: {
    latitude: number;
    longitude: number;
  };
  status: string;
  image: {
    imageUrl: string;
    imageId: string;
  };
  version: number;
}

interface DeliveryModel extends mongoose.Model<DeliveryDoc> {
  build(attrs: DeliveryAttrs): DeliveryDoc;
}

const deliverySchema = new mongoose.Schema(
  {
    delivererId: {
      type: String,
      required: true,
    },
    orderId: {
      type: String,
      required: true,
    },
    clientName: {
      type: String,
      required: true,
    },
    clientAddress: {
      type: String,
      required: true,
    },
    delivererCoordinates: {
      latitude: {
        type: Number,
        required: true,
      },
      longitude: {
        type: Number,
        required: true,
      },
    },
    image: {
      imageUrl: {
        type: String,
        required: false,
      },
      imageId: {
        type: String,
        required: false,
      },
    },
    status: {
      type: String,
      required: true,
      enum: Object.values(DeliveryStatus),
      default: DeliveryStatus.InProgress,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

deliverySchema.set('versionKey', 'version');
deliverySchema.plugin(updateIfCurrentPlugin);

deliverySchema.statics.build = (attrs: DeliveryAttrs) => {
  return new Delivery(attrs);
};

const Delivery = mongoose.model<DeliveryDoc, DeliveryModel>(
  'Delivery',
  deliverySchema
);

export { Delivery };
