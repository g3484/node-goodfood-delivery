import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import { currentUser, errorHandler, NotFoundError } from '@goodfood/common';
import { createDeliveryRouter } from './routes/new';
import { ordersAwaitingDeliveryRouter } from './routes/index';
import { completeDeliveryRouter } from './routes/completed';
import { deleteDeliveryRouter } from './routes/delete';
import { DeliveryByOrderIdRouter } from './routes/show';
import { ordersCompletedDeliveryRouter } from './routes/stats/countDeliveries';
import { updateDeliveryRouter } from './routes/update';
import { DeliveryOrderDetailsRouter } from './routes/details';
import { currentDeliveryRouter } from './routes/current';

const app = express();
app.set('trust proxy', true);
app.use(json());

app.use(currentUser);

app.all('/api/deliveries/health', (req, res) => {
  res.send('K8S is healthy');
});

app.use(createDeliveryRouter);
app.use(ordersAwaitingDeliveryRouter);
app.use(currentDeliveryRouter);
app.use(completeDeliveryRouter);
app.use(deleteDeliveryRouter);
app.use(ordersCompletedDeliveryRouter);
app.use(updateDeliveryRouter);
app.use(DeliveryOrderDetailsRouter);
app.use(DeliveryByOrderIdRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
