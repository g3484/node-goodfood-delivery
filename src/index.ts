require('dotenv').config();

import mongoose from 'mongoose';
import { app } from './app';
import { DatabaseConnectionError } from '@goodfood/common';
import { natsWrapper } from './nats-wrapper';
import { OrderCancelledListener } from './events/listeners/order-cancelled-listener';
import { OrderCreatedListener } from './events/listeners/order-created-listener';
import { DeliveryCreatedListener } from './events/listeners/delivery-created-listener';
import { PaymentCreatedListener } from './events/listeners/payment-created-listener';
import { DeliveryCompletedListener } from './events/listeners/delivery-completed-listener';
import { DeliveryDeletedListener } from './events/listeners/delivery-deleted-listener';

const PORT = 3000;

const start = async () => {
  if (process.env.NODE_ENV === 'test') return;

  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be defined');
  }

  if (!process.env.MONGO_URI) {
    throw new Error('MONGO_URI must be defined');
  }

  if (
    !process.env.CLOUDINARY_NAME ||
    !process.env.CLOUDINARY_KEY ||
    !process.env.CLOUDINARY_SECRET
  ) {
    throw new Error('Cloudinary Env Vars must be defined');
  }

  if (!process.env.NATS_URL) {
    throw new Error('NATS_URL must be defined');
  }

  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error('NATS_CLUSTER_ID must be defined');
  }

  if (!process.env.NATS_CLIENT_ID) {
    throw new Error('NAT_CLIENT_ID must be defined');
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );
    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed!');
      process.exit();
    });
    process.on('SIGINT', () => natsWrapper.client.close());
    process.on('SIGTERM', () => natsWrapper.client.close());

    new OrderCreatedListener(natsWrapper.client).listen();
    new OrderCancelledListener(natsWrapper.client).listen();
    new PaymentCreatedListener(natsWrapper.client).listen();
    new DeliveryCreatedListener(natsWrapper.client).listen();
    new DeliveryCompletedListener(natsWrapper.client).listen();
    new DeliveryDeletedListener(natsWrapper.client).listen();

    await mongoose.connect(process.env.MONGO_URI);
    console.log('Connected to mongoDb');
  } catch (err) {
    throw new DatabaseConnectionError();
  }

  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT} !`);
  });
};

start();
