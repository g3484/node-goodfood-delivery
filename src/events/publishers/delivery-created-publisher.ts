import { Publisher, DeliveryCreatedEvent, Subjects } from '@goodfood/common';

export class DeliveryCreatedPublisher extends Publisher<DeliveryCreatedEvent> {
  subject: Subjects.DeliveryCreated = Subjects.DeliveryCreated;
}
