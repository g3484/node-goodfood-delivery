import { Publisher, DeliveryDeletedEvent, Subjects } from '@goodfood/common';

export class DeliveryDeletedPublisher extends Publisher<DeliveryDeletedEvent> {
  subject: Subjects.DeliveryDeleted = Subjects.DeliveryDeleted;
}
