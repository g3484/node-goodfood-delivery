import { Publisher, DeliveryCompletedEvent, Subjects } from '@goodfood/common';

export class DeliveryCompletedPublisher extends Publisher<DeliveryCompletedEvent> {
  subject: Subjects.DeliveryCompleted = Subjects.DeliveryCompleted;
}
