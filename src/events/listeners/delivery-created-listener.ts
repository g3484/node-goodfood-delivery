import {
  Subjects,
  Listener,
  DeliveryCreatedEvent,
  OrderStatus,
} from '@goodfood/common';
import { Message } from 'node-nats-streaming';
import { queueGroupName } from './queue-group-name';
import { Order } from '../../models/order';

export class DeliveryCreatedListener extends Listener<DeliveryCreatedEvent> {
  subject: Subjects.DeliveryCreated = Subjects.DeliveryCreated;
  queueGroupName = queueGroupName;

  async onMessage(data: DeliveryCreatedEvent['data'], msg: Message) {
    const order = await Order.findById(data.orderId);

    if (!order) {
      throw new Error('Order not found');
    }

    order.set({ status: OrderStatus.DeliveryInProgress });
    await order.save();

    msg.ack();
  }
}
