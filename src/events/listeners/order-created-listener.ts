import { Message } from 'node-nats-streaming';
import { Listener, OrderCreatedEvent, Subjects } from '@goodfood/common';
import { queueGroupName } from './queue-group-name';
import { Delivery } from '../../models/delivery';
import { Order } from '../../models/order';

export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
  readonly subject = Subjects.OrderCreated;
  queueGroupName = queueGroupName;

  async onMessage(data: OrderCreatedEvent['data'], msg: Message) {
    const { id, userId, userName, address, dateOfOrder, status, version } =
      data;

    const order = Order.build({
      id,
      userId,
      userName,
      address,
      dateOfOrder,
      status,
      version,
    });

    await order.save();

    msg.ack();
  }
}
