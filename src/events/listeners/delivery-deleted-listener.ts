import {
  Subjects,
  Listener,
  DeliveryDeletedEvent,
  OrderStatus,
} from '@goodfood/common';
import { Message } from 'node-nats-streaming';
import { queueGroupName } from './queue-group-name';
import { Order } from '../../models/order';

export class DeliveryDeletedListener extends Listener<DeliveryDeletedEvent> {
  subject: Subjects.DeliveryDeleted = Subjects.DeliveryDeleted;
  queueGroupName = queueGroupName;

  async onMessage(data: DeliveryDeletedEvent['data'], msg: Message) {
    const order = await Order.findById(data.orderId);

    if (!order) {
      throw new Error('Order not found');
    }

    order.set({ status: OrderStatus.AwaitingDelivery });
    await order.save();

    msg.ack();
  }
}
