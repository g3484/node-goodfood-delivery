import {
  Subjects,
  Listener,
  DeliveryCompletedEvent,
  OrderStatus,
} from '@goodfood/common';
import { Message } from 'node-nats-streaming';
import { queueGroupName } from './queue-group-name';
import { Order } from '../../models/order';

export class DeliveryCompletedListener extends Listener<DeliveryCompletedEvent> {
  subject: Subjects.DeliveryCompleted = Subjects.DeliveryCompleted;
  queueGroupName = queueGroupName;

  async onMessage(data: DeliveryCompletedEvent['data'], msg: Message) {
    const order = await Order.findById(data.orderId);

    if (!order) {
      throw new Error('Order not found');
    }

    order.set({ status: OrderStatus.Completed });
    await order.save();

    msg.ack();
  }
}
