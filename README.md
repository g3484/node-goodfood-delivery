# GoodFood - NodeJS => Deliveries

## Description

GoodFood est une solution de livraison de plats cuisinés.

Cette solution comporte un site web ainsi qu'une application mobile, et est composée de plusieurs micro services effectuant des actions spécifiques.

Dans ce repository, le microservice NodeJS gère les livraisons de la solution GoodFood.

### Fonctionnalités

Le microservice NodeJS inclus les fonctionnalités suivantes :

- **Lister les commandes (en attente de livraison)** : /api/deliveries
- **Voir le détail d'une commande à livrer selon son identifiant** : /api/deliveries/:orderId
- **Création d'une livraison** : \*/api/deliveries => Les paramètres qu'il y a besoin sont : OrderId, delivererCoordinates
- **Suppression d'une livraison (Livraison Annulée) selon son identifiant** : \*/api/deliveries/:id
- **Update d'une livraison** : \*/api/deliveries/:id (méthode patch) => Les paramètres qu'il y a besoin sont : delivererCoordinates

- **Livraison complétée** : \*/api/deliveries/:id (méthode post) => Les paramètres qu'il y a besoin sont : image

* => Routes privées accessible avec le JWT
